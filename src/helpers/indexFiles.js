const {dirname, basename} = require('path')

module.exports.indexFiles =
function indexFiles (root, index, trailingSlash) {
  const absolute = new Map()
  const relative = new Map()
  const pathnames = new Map()
  for (const filepath of index) {
    const asAbsolute = filepath.replace(/[\\/]+/g, '/')
    const asRelative = asAbsolute.substr(root.length)
    let asPathname
    if (basename(asRelative) === 'index.html') {
      if (trailingSlash === 'always' && asRelative !== '/index.html') {
        asPathname = `${dirname(asRelative)}/`
      } else {
        asPathname = dirname(asRelative)
      }
    } else {
      asPathname = asRelative
    }
    const entry = {
      absolute: asAbsolute,
      relative: asRelative,
      pathname: asPathname
    }
    absolute.set(asAbsolute, entry)
    relative.set(asRelative, entry)
    pathnames.set(asPathname, entry)
  }
  return {relative, absolute, pathnames}
}
