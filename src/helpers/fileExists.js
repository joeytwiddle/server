const {statSync} = require('fs')

module.exports.fileExists = (path) => {
  try {
    const stats = statSync(path)
    return stats.isFile()
  } catch (err) {
    return false
  }
}
