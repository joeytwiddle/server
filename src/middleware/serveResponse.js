const Negotiator = require('negotiator')
const {createReadStream, stat} = require('fs')
const {promisify} = require('util')
const {buildHeaders} = require('../helpers/buildHeaders')
const {isLoopback} = require('../helpers/isLoopback')
const {sync: readPkgUp} = require('read-pkg-up')
const osName = require('os-name')

function noop () {}

function serverHeader () {
  const {pkg} = readPkgUp({cwd: __dirname})
  const [name] = Object.keys(pkg.bin)
  const {version} = pkg
  const {node, nghttp2} = process.versions
  const products = [
    `${name} ${version}`,
    `node ${node}`,
    `nghttp2 ${nghttp2}`,
    `${osName()}`
  ]
  return products.join(', ')
}
const HEADER_SERVER = serverHeader()

module.exports.serveResponse = ({signature}) =>
async function serveResponse (request, response, next) {
  const {resolved, headers} = buildHeaders(
    request.resolved,
    request.options.cacheControl.immutable,
    new Negotiator(request).encodings(),
    request.fileIndex
  )

  if (signature === true) {
    headers.server = HEADER_SERVER
  }

  if (!isLoopback(request)) {
    headers['strict-transport-security'] =
      `max-age=${request.options.strictTransportSecurity.maxAge}`
  }

  const statusCode = response.statusCode || 200

  if (request.httpVersionMajor === 2) {
    // HTTP/2 GET
    request.once('error', noop)
    response.once('error', noop)
    response.stream.once('error', () => {
      if (response.stream.destroyed === false) {
        response.stream.respond({
          ':status': 500,
          'content-type': 'text/plain'
        })
        response.stream.end('Internal Server Error')
      }
    })
    if (statusCode !== 200) {
      headers[':status'] = statusCode
    }
    response.stream.respondWithFile(resolved.absolute, headers, {
      statCheck (stat, headers) {
        if (request.method === 'HEAD') {
          // HTTP/2 HEAD
          response.stream.respond(headers)
          return false
        }
      }
    })
    next()
  } else {
    if (request.method === 'HEAD') {
      // HTTP/1 HEAD
      response.writeHead(statusCode, headers)
      response.flushHeaders()
      response.end()
      next()
    } else {
      // HTTP/1 GET
      const file = createReadStream(resolved.absolute)
      file.once('open', async () => {
        try {
          response.setHeader(
            'content-length',
            (await promisify(stat)(resolved.absolute)).size
          )
        } catch (error) {
          return next(error)
        }
        response.writeHead(statusCode, headers)
        file.pipe(response)
        next()
      })
      file.once('error', (error) => {
        file.removeAllListeners()
        if (!response.headersSent) next(error)
        else if (!response.finished) response.end()
      })
      file.once('end', () => {
        file.removeAllListeners()
      })
    }
  }
}
