const {MethodNotAllowed} = require('http-errors')

module.exports.allowedMethods = (methods) => {
  return (request, response, next) => {
    if (methods.includes(request.method)) next()
    else next(new MethodNotAllowed())
  }
}
