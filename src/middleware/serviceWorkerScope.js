module.exports.serviceWorkerScope = () => {
  return (request, response, next) => {
    const scope = request.options.serviceWorker.allowed
    if (scope && request.headers['service-worker'] === 'script') {
      response.setHeader('service-worker-allowed', scope)
    }
    next()
  }
}
